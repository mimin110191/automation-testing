const {Builder,By, Key, until, error, Capabilities} = require("selenium-webdriver");
const {assert, expect} = require('chai');
let capabilities = Capabilities.chrome();

describe('Test Home Page',function () {
    let driver
    beforeEach(async () => {
        driver = await new Builder()
            .usingServer(process.env.SELENIUM_URL)
            .withCapabilities(capabilities)
            .build();
    }, 30000)
    it('Test about section', async function() {
        try {
            await driver.get('https://namnguyen.io/');

            let aboutSection = await driver
                .findElement(By.className('animate-waving-hand'))
                .getText();

            assert.equal(aboutSection,'👋🏻');
        } finally {
            await driver.quit();
        }
    })
    it('Test career section ',async function(){
        try {
            await driver.get('https://namnguyen.io/');

            let careerSection = await driver
                // .findElement(By.className('text-4xl mb-4 font-normal text-black'));
                //assert.equal(section2, 'My Career So Far');
                .findElement(By.xpath("//*[text() = 'My Career So Far']"));

            expect(careerSection).to.exist;
        } finally {
            await driver.quit();
        }
    })
    it('Test sendmail section', async function(){
        try{
            await driver.get('https://namnguyen.io/');

            let sendmailSection = await driver
                .findElement(By.xpath("//*[text() ='Send me a message']"));

            expect(sendmailSection).to.exist;

        }finally {
            await driver.quit();
        }
    })
})