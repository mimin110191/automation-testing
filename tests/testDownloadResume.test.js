const{Builder, By, Capabilities} = require('selenium-webdriver');
const{assert}= require('chai');
let capabilities = Capabilities.chrome();

describe('Test download function',function (){
    let driver
    beforeEach(async () => {
        driver = await new Builder()
            .usingServer(process.env.SELENIUM_URL)
            .withCapabilities(capabilities)
            .build();
    }, 30000)
    it('Test download resume',async function(){
        const originalWindow = await driver.getWindowHandle();

        try{
            await driver.get('https://namnguyen.io/');
            await driver
                 .findElement(By.className('sm:mt-0 uppercase text-sm font-bold tracking-wide bg-cyan-600 p-3 rounded-lg focus:outline-none focus:shadow-outline text-white')).click();

            await driver.wait(
                async () => (await driver.getAllWindowHandles()).length === 2,
                10000);

            const windows = await driver.getAllWindowHandles();
            for (const handle of windows) {
                if (handle !== originalWindow) {
                    await driver.switchTo().window(handle);

                }
            }
            const title = await driver.getCurrentUrl();

            assert.equal(title,'https://namnguyen.io/alex-nguyen.pdf')
        }finally {
            await driver.quit();
        }
    })
})